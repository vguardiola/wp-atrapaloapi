<?php
/**
 * File that do a proxy for the autocomplete
 * @author Victor Guardiola <victor.guardiola@gmail.com>
 * @package Wordpress
 * @subpacke Plugin
 * @filesource
 */

$options['api_domain'] = "atrapalo.com";
if (!$_GET['f']) {
    exit;
}
if (strpos($_SERVER['HTTP_REFERER'], $_SERVER['HTTP_HOST']) === false) {
    exit;
}
$src = "http://www.".$options['api_domain'] . $_GET['f'];
$curl_handle = curl_init();
$options = array(
    CURLOPT_URL => $src,
    CURLOPT_HEADER => false,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_FOLLOWLOCATION => true,
);
curl_setopt_array($curl_handle, $options);
echo $data = curl_exec($curl_handle);
