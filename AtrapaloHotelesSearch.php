<?php
/**
 * @author Victor Guardiola <victor.guardiola@gmail.com>
 * @package Wordpress
 * @subpacke Plugin
 * @filesource
 */

/**
 * Description of AtrapaloHotelesSearch
 *
 * @author Victor Guardiola <victor.guardiola@gmail.com>
 */
class AtrapaloHotelesSearch
{
    /**
     *
     * @var string
     */
    public $xml = '';
    /**
     *
     * @var string
     */
    public $xml_res = '';
    /**
     *
     * @var string
     */
    public $url_api = 'https://api.atrapalo.com/hoteles/availability';

    /**
     *
     * @param mixed $atts
     * @param mixed $vars
     */
    public function doRequest($atts, $vars)
    {
        $this->vars = $vars;
        $vars['fecha_entrada'] = explode("/", $vars['fecha_entrada']);
        $vars['fecha_salida'] = explode("/", $vars['fecha_salida']);
        if ($atts['api_domain'] != '') {
            $this->url_api = str_replace('atrapalo.com', $atts['api_domain'], $this->url_api);
        }
        $this->xml = '<?xml version="1.0" encoding="utf-8"?>';
        $this->xml .= '<OTA_HotelAvailRQ xmlns="http://www.opentravel.org/OTA/2003/05"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://www.atrapalo.com/api/schemas/OTA/2007B/OTA_HotelAvailRQ.xsd"
            Version="1.0" EchoToken="wp_api_atr_plug"
            Target="Prod" PrimaryLangID="' . $atts['api_pri_lang'] . '"
            AltLangID="' . $atts['api_sec_lang'] . '"
            MaxResponses="' . $atts['api_max_results'] . '">';
        $this->xml .= '<POS>';
        $this->xml .= '<Source>';
        $this->xml .= '<RequestorID
            MessagePassword="' . $atts['api_pass'] . '"
            Type="5" ID="' . $atts['api_user'] . '"/>';
        $this->xml .= '</Source>';
        $this->xml .= '</POS>';
        $this->xml .= '<AvailRequestSegments>';
        $this->xml .= '<AvailRequestSegment>';
        $this->xml .= '<StayDateRange Start="' . $vars['fecha_entrada'][2] . '-' . $vars['fecha_entrada'][1] . '-' . $vars['fecha_entrada'][0] . '"';
        $this->xml .= ' End="' . $vars['fecha_salida'][2] . '-' . $vars['fecha_salida'][1] . '-' . $vars['fecha_salida'][0] . '"/>';
        $this->xml .= '<RoomStayCandidates>';
        $this->xml .= '<RoomStayCandidate Quantity="' . $vars['num_habitaciones'] . '">';
        $this->xml .= '<GuestCounts>';
        $this->xml .= '<GuestCount Count="' . $vars['num_adultos_1'] . '"/>';
        $this->xml .= '</GuestCounts>';
        $this->xml .= '</RoomStayCandidate>';
        $this->xml .= '</RoomStayCandidates>';
        $this->xml .= '<HotelSearchCriteria>';
        $this->xml .= '<Criterion>';
        $this->xml .= '<Address>';
        $this->xml .= '<CityName>' . $vars['nombre_destino'] . '</CityName>';
        $this->xml .= '<StateProv></StateProv>';
        $this->xml .= '<CountryName>' . $vars['nombre_pais'] . '</CountryName>';
        $this->xml .= '</Address>';
        $this->xml .= '</Criterion>';
        $this->xml .= '</HotelSearchCriteria>';
        $this->xml .= '</AvailRequestSegment>';
        $this->xml .= '</AvailRequestSegments>';
        $this->xml .= '</OTA_HotelAvailRQ>';
    }

    /**
     * Metodo que envia la petición XML a los servidores de atrapalo
     */
    public function sendRequest()
    {
        $md5 = md5($this->xml);
        $cache_file = WPATR_PLUGIN_DIR . '/cache/' . $md5 . '.cache';
        if (file_exists($cache_file) && time() - filemtime($cache_file) < 3600) {
            $this->xml_res = file_get_contents($cache_file);
        } else {
            if (file_exists($cache_file)) {
                unlink($cache_file);
            }
            $curl_handle = curl_init();
            $options = array(CURLOPT_URL => $this->url_api, CURLOPT_HEADER => false, CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FOLLOWLOCATION => true, CURLOPT_POST => false, CURLOPT_POSTFIELDS => $this->xml,
                CURLOPT_USERAGENT => "wp_atr_api_plugin", CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_HTTPHEADER => array(0 => 'Pragma: no-cache', 1 => 'Content-Type:text/xml;Accept-encode:UTF-8',
                    2 => 'Expect:'));
            curl_setopt_array($curl_handle, $options);
            $this->xml_res = curl_exec($curl_handle);
            file_put_contents($cache_file, $this->xml_res);
            curl_close($curl_handle);
        }
        $this->xml_res = @new SimpleXMLElement(str_replace('xmlns', 'nsxml', $this->xml_res));
        if (count($this->xml_res->Errors->Error)>0) {
//                echo "<pre>".print_r($this->xml_res,1)."</pre>";
//                echo "<pre>".print_r($this->xml,1)."</pre>";
        }
    }

    /**
     * Metodo que procesa el resultado y muestra
     * @param $vars
     */
    public function processResult($vars)
    {
        try {
            $s_xml = $this->xml_res;
            $vars = $this->vars;
            if ($s_xml->HotelStays->HotelStay) {
                $mapData = array();
                $hotels = array();
                foreach ($s_xml->HotelStays->HotelStay as $hotelStay) {
                    $atrBasicPropertyInfo = $hotelStay->BasicPropertyInfo->attributes();
                    $hotelCode = (int)$atrBasicPropertyInfo['HotelCode'];
                    $atrPosition = $hotelStay->BasicPropertyInfo->Position->attributes();
                    $aRoomStay = $s_xml->xpath('//RoomStay[@RPH=' . $hotelCode . ']');
                    $aHotelAmenities = $s_xml->xpath('//HotelAmenities[@HotelAmenitiesRPH=' . $hotelCode . ']');
                    list($HotelInfoLink) = $s_xml->xpath('//HotelInfoLink[@RPH=' . $hotelCode . ']');
                    list($ReservationLink) = $s_xml->xpath('//ReservationLink[@RPH=' . $hotelCode . ']');
                    $hotels[] = array("HotelCode" => $hotelCode,
                        "HotelName" => (string)$atrBasicPropertyInfo['HotelName'],
                        "HotelCat" => $this->toStars((string)$atrBasicPropertyInfo['HotelSegmentCategoryCode']),
                        "CityName" => (string)$hotelStay->BasicPropertyInfo->Address->CityName,
                        "StateProv" => (string)$hotelStay->BasicPropertyInfo->Address->StateProv,
                        "CountryName" => (string)$hotelStay->BasicPropertyInfo->Address->CountryName,
                        "HotelText" => strip_tags((string)$hotelStay->BasicPropertyInfo->VendorMessages->VendorMessage->SubSection->Paragraph->Text),
                        "aRoomStay" => $aRoomStay, "aHotelAmenities" => $aHotelAmenities,
                        "HotelInfoLink" => $HotelInfoLink, "ReservationLink" => $ReservationLink);
                    $mapData[] = array("lat" => (float)$atrPosition['Latitude'],
                        "lon" => (float)$atrPosition['Longitude'], "hotel" => (string)$atrBasicPropertyInfo['HotelName'], "link" => $HotelInfoLink);
                }
                include WPATR_PLUGIN_DIR . '/xhtml/plantilla.phtml';
            } else {
                echo __("No hay resultados", "atrapalo");
            }
        } catch (Exception $e) {
            echo __("No hay resultados", "atrapalo");
        }
    }

    public function toStars($cat)
    {
        if (preg_match('|\dE|i', $cat)) {
            $s = substr($cat, 0, 1);
            $cat = str_repeat('*',$s);
        }
        return $cat;

    }

}
