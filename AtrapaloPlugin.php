<?php
/**
 * Plugin Name: AtrapaloPlugin
 * Plugin URI: https://bitbucket.org/vguardiola/wp-atrapaloapi
 * Description: Plugin with widget to search bookings in Atrapalo.com.
 *
 * @author Victor Guardiola <victor.guardiola@gmail.com>
 * @version 0.1
 */
define('WPATR_VERSION', '0.5');
if (!defined('WPATR_PLUGIN_BASENAME')) {
    define('WPATR_PLUGIN_BASENAME', plugin_basename(__FILE__));
}
if (!defined('WPATR_PLUGIN_NAME')) {
    define('WPATR_PLUGIN_NAME', trim(dirname(WPATR_PLUGIN_BASENAME), '/'));
}
if (!defined('WPATR_PLUGIN_DIR')) {
    define('WPATR_PLUGIN_DIR', WP_PLUGIN_DIR . '/' . WPATR_PLUGIN_NAME);
}
if (!defined('WPATR_PLUGIN_DB_OPTION')) {
    define('WPATR_PLUGIN_DB_OPTION', 'AtrapaloApi_options');
}
/**

 */class WPAtrapaloAdminOptions
{
    /**
     * Method that add an option in the WP's Menu
     */
    public function admin_menu()
    {
        add_options_page('Atrapalo', 'Atrapalo', 8, basename(__FILE__), array($this, 'handle_options'));
    }

    /**
     * @return array
     */
    public static function get_options()
    {
        $options = array('api_user' => '', 'api_pass' => '', 'api_page' => '', 'api_page_res' => '',
                         'api_domain' => 'atrapalo.com', 'api_num_results' => 20, 'api_max_results' => 300,
                         'api_pri_lang' => 'es_ES', 'api_sec_lang' => 'en_EN');
        $saved = get_option(WPATR_PLUGIN_DB_OPTION);
        if (!empty($saved)) {
            foreach ($saved as $key => $option) {
                $options[$key] = $option;
            }
        }
        if ($saved != $options) {
            update_option(WPATR_PLUGIN_DB_OPTION, $options);
        }
        return $options;
    }

    /**
     */
    function install()
    {
        self::get_options();
    }

    /**
     */
    public function handle_options()
    {
        if (isset($_POST['submitted'])) {
            check_admin_referer('atrpalo-api-nonce');
            $atts = array();
            $atts['api_user'] = htmlspecialchars($_POST['api_user']);
            $atts['api_pass'] = htmlspecialchars($_POST['api_pass']);
            $atts['api_domain'] = htmlspecialchars($_POST['api_domain']);
            $atts['api_page'] = (int)$_POST['api_page'];
            $atts['api_page_res'] = (int)$_POST['api_page_res'];
            $atts['api_num_results']
                = (int)$_POST['api_num_results'] > 0 ? (int)$_POST['api_num_results'] : $atts['api_num_results'];
            $atts['api_max_results']
                = (int)$_POST['api_max_results'] > 0 ? (int)$_POST['api_max_results'] : $atts['api_max_results'];
            $atts['api_pri_lang'] = $_POST['api_pri_lang'];
            $atts['api_sec_lang'] = $_POST['api_sec_lang'];
            update_option(WPATR_PLUGIN_DB_OPTION, $atts);
            echo '<div class="updated fade"><p>'.__("Plugin settings saved.").'</p></div>';
        }
        include(WPATR_PLUGIN_DIR . '/xhtml/AtrapaloAdminPage.php');
    }

}

/**

 */
class WPWidgetAtrapaloHoteles extends WP_Widget
{
    /**

     */
    public function WPWidgetAtrapaloHoteles()
    {
        $widget_ops = array('description' => __('Busqueda en el API de Hoteles de Atrapalo.com', 'atrapalo'));
        $this->WP_Widget('Busqueda Atrapalo.com', __('Busqueda Atrapalo.com', 'atrapalo'), $widget_ops);
    }

    /**
     * @param array $args
     * @param array $instance
     * @return void
     */
    public function widget($args, $instance)
    {
        $options = WPAtrapaloAdminOptions::get_options();
        if ((int)$options['api_page']) {
            $action = get_page_link($options['api_page']);
        }
        if ($action == '') {
            $action = "/resultados/";
        }
        include WPATR_PLUGIN_DIR . "/xhtml/widget.phtml";
    }

    /**
     * @param $atts
     * @param null $content
     * @param string $code
     */
    public function embedResult($atts, $content = null, $code = "")
    {
        require_once WPATR_PLUGIN_DIR . '/AtrapaloHotelesSearch.php';
        $req = new AtrapaloHotelesSearch();
        $op = WPAtrapaloAdminOptions::get_options();
        $req->doRequest($op, $_POST);
        $req->sendRequest();
        $req->processResult($op, $_POST);
    }

}

/**

 */
class WPWidgetAtrapaloRestaurantes extends WP_Widget
{
    /**

     */
    public function WPWidgetAtrapaloRestaurantes()
    {
        $widget_ops = array('description' => __('Busqueda en el API de Restaurantes de Atrapalo.com', 'atrapalo'));
        $this->WP_Widget(
            'Busqueda Restaurantes Atrapalo.com', __('Busqueda Restaurantes Atrapalo.com', 'atrapalo'), $widget_ops
        );
    }

    /**
     * @param array $args
     * @param array $instance
     * @return void
     */
    public function widget($args, $instance)
    {
        $options = WPAtrapaloAdminOptions::get_options();
        if ((int)$options['api_page']) {
            $action = get_page_link($options['api_page']);
        }
        if ($action == '') {
            $action = "/resultados/";
        }
        include WPATR_PLUGIN_DIR . "/xhtml/widget_rest.xhtml";
    }

    /**
     * @param $atts
     * @param null $content
     * @param string $code
     */
    public function embedResult($atts, $content = null, $code = "")
    {
        require_once WPATR_PLUGIN_DIR . '/AtrapaloRestaurantesSearch.php';
        $req = new AtrapaloRestaurantesSearch();
        $req->doRequest($atts, $_POST);
        $req->sendRequest();
        $req->processResult($op, $_POST);
    }
}

/**
 * Función que ejeuta lo necesario para ejecutar los widgets
 */
function WPAtrapaloAdminOtionsInit()
{
    register_widget('WPWidgetAtrapaloHoteles');
    register_widget('WPWidgetAtrapaloRestaurantes');
    $AtrapaloApi = new WPAtrapaloAdminOptions();
    add_action('admin_menu', array($AtrapaloApi, 'admin_menu'));
    add_shortcode('atr_rs_hot', array("WPWidgetAtrapaloHoteles", 'embedResult'));
    add_shortcode('atr_res', array("WPWidgetAtrapaloHoteles", 'embedResult'));
    add_shortcode('atr_rs_res', array("WPWidgetAtrapaloRestaurantes", 'embedResult'));
}
//Registramos el widget
add_action('widgets_init', 'WPAtrapaloAdminOtionsInit');