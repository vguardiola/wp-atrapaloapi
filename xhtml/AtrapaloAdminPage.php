<?php
$atts = $this->get_options();
$action_url = $_SERVER['REQUEST_URI'];
$paginasDisponibles = get_pages();
?>

<div class="wrap" style="max-width:950px !important;">
    <h2>Atrapalo API Affiliates</h2>

    <div id="poststuff" style="margin-top:10px;">
        <div id="sideblock" style="float:right;width:220px;margin-left:10px;">
            <h2>Information</h2>

            <div id="dbx-content" style="text-decoration:none;">
                <img src="http://affiliates.atrapalo.com/favicon.ico" alt="Afilliates"/>
                <a style="text-decoration:none;" href="http://affiliates.atrapalo.com/">Affiliates Atrapalo Home</a><br/><br/>
                <img src="http://www.atrapalo.com/favicon.ico" alt="Atrapalo"/>
                <a style="text-decoration:none;" href="http://www.atrapalo.com/">Atrapalo.com</a><br/><br/>
            </div>
        </div>
        <div id="mainblock" style="width:710px">
            <div class="dbx-content">
                <form name="AtraplaoAffiliates" action="<?php echo $action_url ?>" method="post">
                    <input type="hidden" name="submitted" value="1"/>
                    <?php wp_nonce_field('atrpalo-api-nonce'); ?>
                    <h2>Usage</h2>

                    <p><?php echo __("Primero has de darte de alta en el programa de");?>
                        <a style="text-decoration:none;" href="http://affiliates.atrapalo.com/"><?php echo __(
                            "afiliados"
                        );?></a> <?php echo __("de Atrapalo.com");?>
                    </p>

                    <p>Segundo has de pedir acceso a la API de busqueda de hoteles.</p>

                    <p>Tercero rellenar el siguiente formulario.</p>
                    <br/>

                    <h2>Api User/Pass</h2>

                    <p>Fill whith the user / pass</p>
                    <label for="api_user"> User</label><input type="text" name="api_user" id="api_user"
                                                              value="<?php echo $atts['api_user'] ?>"/><br/>
                    <label for="api_pass"> Pass</label><input type="password" name="api_pass" id="api_pass"
                                                              value="<?php echo $atts['api_pass'] ?>"/><br/>
                    <br/>

                    <h2>Display</h2>
                    <label for="api_page"> Page with display tag ([atr_rs_hot])</label><br/>
                    <select name="api_page" id="api_page">
                        <?php
                        for ($i = 0; $i < count($paginasDisponibles); $i++) {
                            echo '<option value="' . $paginasDisponibles[$i]->ID . '" ';
                            echo ($atts['api_page'] == $paginasDisponibles[$i]->ID ? 'selected="selected"' : '');
                            echo '>' . $paginasDisponibles[$i]->post_title . '</option>';
                        }
                        ?>
                    </select><br/>
                    <label for="api_page_res"> Page with display tag ([atr_rs_res])</label><br/>
                    <select name="api_page_res" id="api_page_res">
                        <?php
                        for ($i = 0; $i < count($paginasDisponibles); $i++) {
                            echo '<option value="' . $paginasDisponibles[$i]->ID . '" ';
                            echo ($atts['api_page'] == $paginasDisponibles[$i]->ID ? 'selected="selected"' : '');
                            echo '>' . $paginasDisponibles[$i]->post_title . '</option>';
                        }
                        ?>
                    </select><br/>
                    <label for="api_num_results"> # Results/Page</label><br/>
                    <input type="text" name="api_num_results" id="api_num_results"
                           value="<?php echo $atts['api_num_results'] ?>"/><br/>
                    <br/>
                    <label for="api_max_results"> Max # Results</label><br/>
                    <input type="text" name="api_max_results" id="api_max_results"
                           value="<?php echo $atts['api_max_results'] ?>"/><br/>
                    <br/>
                    <label for="api_pri_lang"> Primary Language</label><br/>
                    <select name="api_pri_lang" id="api_pri_lang">
                        <option <?=$atts['api_pri_lang'] == 'en_EN' ? 'selected="selected"' : ''; ?>>en_EN</option>
                        <option <?=$atts['api_pri_lang'] == 'es_ES' ? 'selected="selected"' : ''; ?>>es_ES</option>
                        <option <?=$atts['api_pri_lang'] == 'pt_PT' ? 'selected="selected"' : ''; ?>>pt_PT</option>
                        <option <?=$atts['api_pri_lang'] == 'de_DE' ? 'selected="selected"' : ''; ?>>de_DE</option>
                    </select>
                    <br/>
                    <label for="api_sec_lang"> Secondary Language</label><br/>
                    <select name="api_sec_lang" id="api_sec_lang">
                        <option <?=$atts['api_sec_lang'] == 'en_EN' ? 'selected="selected"' : ''; ?>>en_EN</option>
                        <option <?=$atts['api_sec_lang'] == 'es_ES' ? 'selected="selected"' : ''; ?>>es_ES</option>
                        <option <?=$atts['api_sec_lang'] == 'pt_PT' ? 'selected="selected"' : ''; ?>>pt_PT</option>
                        <option <?=$atts['api_sec_lang'] == 'de_DE' ? 'selected="selected"' : ''; ?>>de_DE</option>
                    </select>
                    <br/>
                    <label for="api_domain">Api Domain</label><br/>
                    <select name="api_domain" id="api_domain">
                        <option <?=$atts['api_domain'] == 'atrapalo.com' ? 'selected="selected"' : ''; ?>>atrapalo.com
                        </option>
                        <option <?=$atts['api_domain'] == 'atrapalo.it' ? 'selected="selected"' : ''; ?>>atrapalo.it
                        </option>
                        <option <?=$atts['api_domain'] == 'atrapalo.cl' ? 'selected="selected"' : ''; ?>>atrapalo.cl
                        </option>
                        <option <?=$atts['api_domain'] == 'atrapalo.com.br' ? 'selected="selected"' : ''; ?>>
                            atrapalo.com.br
                        </option>
                        <option <?=$atts['api_domain'] == 'atrapalo.com.co' ? 'selected="selected"' : ''; ?>>
                            atrapalo.com.co
                        </option>
                        <option <?=$atts['api_domain'] == 'atrapalo.pe' ? 'selected="selected"' : ''; ?>>atrapalo.pe
                        </option>
                    </select>
                    <br/>

                    <div class="submit"><input type="submit" name="Submit" value="Update"/></div>
                </form>
            </div>
        </div>
    </div>
</div>